import React from 'react'
import { Root, Routes } from 'react-static'
import { Link } from '@reach/router'
import 'css/bootstrap.min.css'
import 'css/datatables.min.css'
import 'css/animate.css'

function App() {
  return (
    <Root>
      <div className="content">
        <Routes />
      </div>
    </Root>
  )
}

export default App
