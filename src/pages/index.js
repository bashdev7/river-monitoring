import React, { Component } from 'react';
import { Button, Navbar, Card, Modal, InputGroup, FormControl, Table, ProgressBar, Dropdown } from 'react-bootstrap';
import axios from 'axios';
import _ from 'lodash';
import { Line } from 'react-chartjs-2';
import { Head, withSiteData } from 'react-static';
import randomColor from 'random-material-color'
import sma from 'sma'
import uuidv4 from 'uuid/v4'
import async from 'async'
import { concat } from 'rxjs';

// 30 Random colors

const rangeColor = _.range(0, 30)

const colorStates = _.map(rangeColor, () => randomColor.getColor())

export default class Report extends Component {
  constructor(props) {
    super(props)
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
      selectedFile: null,
      uploading: false,
      progress: 0,
      dataLoaded: false,
      datesSet: null,
      DOSet: null,
      BODSet: null,
      TSSSet: null,
      TempSet: null,
      TDSSet: null,
      pHSet: null,
      ColorSet: null,
      FecalColiformSet: null,
      TotalColiformSet: null,
      DOSetSMA: null,
      BODSetSMA: null,
      TSSSetSMA: null,
      TempSetSMA: null,
      TDSSetSMA: null,
      pHSetSMA: null,
      ColorSetSMA: null,
      FecalColiformSetSMA: null,
      TotalColiformSetSMA: null,
      activeFilter: null,
    };
  }

  fileSelect = event => {
    this.setState({ selectedFile: event.target.files[0] })
  }

  handleClose() {
    this.setState({ show: false, uploading: false });
  }

  simplifyValues = data => new Promise((resolve, reject) => resolve(_.map(data, o => o)))

  handleUpload = async () => {
    const app = this
    app.setProgress(35)
    app.fileUpload().then(res => {
      console.log(res.data)
      app.setProgress(57)

      const datesSet = this.simplifyValues(_.map(res.data.values.dates[0]))
      const DOSet = this.simplifyValues(_.map(res.data.values.DOTargets[0]))
      const BODSet = this.simplifyValues(_.map(res.data.values.BODTargets[0]))
      const TSSSet = this.simplifyValues(_.map(res.data.values.TSSTargets[0]))
      const TempSet = this.simplifyValues(_.map(res.data.values.TempTargets[0]))
      const TDSSet = this.simplifyValues(_.map(res.data.values.TDSTargets[0]))
      const pHSet = this.simplifyValues(_.map(res.data.values.pHTargets[0]))
      const ColorSet = this.simplifyValues(_.map(res.data.values.ColorTargets[0]))
      const FecalColiformSet = this.simplifyValues(_.map(res.data.values.FecalColiformTargets[0]))
      const TotalColiformSet = this.simplifyValues(_.map(res.data.values.TotalColiformTargets[0]))

      async.waterfall([callback => {
        Promise.all([
          datesSet,
          DOSet,
          BODSet,
          TSSSet,
          TempSet,
          TDSSet,
          pHSet,
          ColorSet,
          FecalColiformSet,
          TotalColiformSet
        ]).then(dataSet => {
          app.setState({
            datesSet: dataSet[0],
            DOSet: dataSet[1],
            BODSet: dataSet[2],
            TSSSet: dataSet[3],
            TempSet: dataSet[4],
            TDSSet: dataSet[5],
            pHSet: dataSet[6],
            ColorSet: dataSet[7],
            FecalColiformSet: dataSet[8],
            TotalColiformSet: dataSet[9],
            dataLoaded: true,
          })
          app.setProgress(70)
          callback(null, 'loadedData')
        })
      }], (err, res) => {
        if (res) {
          app.setProgress(80)
          this.calcSMA()
          app.setProgress(100)
          app.handleClose()
          app.setProgress(0)
        }
      })
    })
  }

  processData = async dataSet => await new Promise(((resolve, reject) => {
    resolve(_.mapValues(dataSet, (o) => {
      return _.values(_.mapValues(_.values(o), function (p) {
        const value = p[Object.keys(p)[0]]
        return value
      }))
    })[0])
  }))

  handleShow() {
    this.setState({ show: true });
  }

  setProgress = number => {
    this.setState({ progress: number })
  }

  fileUpload = async () => {
    const app = this;
    app.setState({ uploading: true })
    this.setProgress(10)
    const formData = new FormData();
    const { selectedFile } = this.state
    formData.append('file', selectedFile);
    this.setProgress(20)
    return await axios.post('http://localhost/river-monitoring/backend/upload.php', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }

  setFilter = name => {
    this.setState({ activeFilter: name })
  }

  resetFilter = () => {
    this.setState({ activeFilter: null })
  }

  calcSMA = () => {
    const {
      DOSet,
      BODSet,
      TSSSet,
      TempSet,
      TDSSet,
      pHSet,
      ColorSet,
      FecalColiformSet,
      TotalColiformSet,
    } = this.state
    this.setState({
      DOSetSMA: _.map(sma(DOSet, 4), o => parseFloat(o)),
      BODSetSMA: _.map(sma(BODSet, 4), o => parseFloat(o)),
      TSSSetSMA: _.map(sma(TSSSet, 4), o => parseFloat(o)),
      TempSetSMA: _.map(sma(TempSet, 4), o => parseFloat(o)-25),
      TDSSetSMA: _.map(sma(TDSSet, 4), o => parseFloat(o)),
      pHSetSMA: _.map(sma(pHSet, 4), o => parseFloat(o)),
      ColorSetSMA: _.map(sma(ColorSet, 4), o => parseFloat(o)),
      FecalColiformSetSMA: _.map(sma(FecalColiformSet, 4), o => parseFloat(o)),
      TotalColiformSetSMA: _.map(sma(TotalColiformSet, 4), o => parseFloat(o)),
    })

  }

  render() {
    const { progress, selectedFile, uploading, dataLoaded,
      datesSet,
      DOSet,
      BODSet,
      TSSSet,
      TempSet,
      TDSSet,
      pHSet,
      ColorSet,
      FecalColiformSet,
      TotalColiformSet,
      activeFilter,
      DOSetSMA,
      BODSetSMA,
      TSSSetSMA,
      TempSetSMA,
      TDSSetSMA,
      pHSetSMA,
      ColorSetSMA,
      FecalColiformSetSMA,
      TotalColiformSetSMA,
    } = this.state
    const datasetsRaw = [
      {
        label: 'DO',
        data: DOSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[0]
        ],
        borderWidth: 1
      },
      {
        label: 'BOD',
        data: BODSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[1]
        ],
        borderWidth: 1
      },
      {
        label: 'TSS',
        data: TSSSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[2]
        ],
        borderWidth: 1
      },
      {
        label: 'Temp',
        data: TempSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[3]
        ],
        borderWidth: 1
      },
      {
        label: 'TDS',
        data: TDSSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[4]
        ],
        borderWidth: 1
      },
      {
        label: 'pH',
        data: pHSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[5]
        ],
        borderWidth: 1
      },
      {
        label: 'Color',
        data: ColorSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[6]
        ],
        borderWidth: 1
      },
      {
        label: 'Fecal Coliform',
        data: FecalColiformSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[7]
        ],
        borderWidth: 1
      },
      {
        label: 'Total Coliform',
        data: TotalColiformSet,
        backgroundColor: [
          'rgba(255,99,132,0)',
        ],
        borderColor: [
          colorStates[8]
        ],
        borderWidth: 1
      },
    ]
    const chartDataOverAll = {
      labels: datesSet,
      datasets: activeFilter ? _.filter(datasetsRaw, ['label', activeFilter]) : datasetsRaw
    }

    const chartDataDOSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'DO',
          data: DOSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[0]
          ],
          borderWidth: 1
        },
        {
          label: 'DO SMA',
          data: DOSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[10]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataBODSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'BOD',
          data: BODSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[1]
          ],
          borderWidth: 1
        },
        {
          label: 'BOD SMA',
          data: BODSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[11]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataTSSSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'TSS',
          data: TSSSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[2]
          ],
          borderWidth: 1
        },
        {
          label: 'TSS SMA',
          data: TSSSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[12]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataTempSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'Temp',
          data: DOSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[3]
          ],
          borderWidth: 1
        },
        {
          label: 'Temp SMA',
          data: TempSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[13]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataTDSSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'TDS',
          data: TDSSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[4]
          ],
          borderWidth: 1
        },
        {
          label: 'TDS SMA',
          data: TDSSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[14]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDatapHMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'pH',
          data: pHSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[5]
          ],
          borderWidth: 1
        },
        {
          label: 'pH SMA',
          data: pHSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[15]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataColorSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'Color',
          data: ColorSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[6]
          ],
          borderWidth: 1
        },
        {
          label: 'Color SMA',
          data: ColorSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[16]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataFecalColiformSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'Fecal Coliform',
          data: FecalColiformSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[7]
          ],
          borderWidth: 1
        },
        {
          label: 'Fecal Coliform SMA',
          data: FecalColiformSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[17]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataTotalColiformSMA = {
      labels: datesSet,
      datasets: [
        {
          label: 'Total Coliform',
          data: TotalColiformSet,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[8]
          ],
          borderWidth: 1
        },
        {
          label: 'Total Coliform SMA',
          data: TotalColiformSetSMA,
          backgroundColor: [
            'rgba(255,99,132,0)',
          ],
          borderColor: [
            colorStates[18]
          ],
          borderWidth: 1
        }
      ]
    }

    const chartDataBlue = {
      labels: datesSet,
      datasets: [{
        label: 'Forecasted Data',
        data: DOSet,
        backgroundColor: [
          'rgba(54, 162, 235,0)',
        ],
        borderColor: [
          'rgb(54, 162, 235)',
        ],
        borderWidth: 1
      }]
    }
    return (
      <div style={{ textAlign: 'left' }} >
        <Head>
          <title>Water Quality Forecasting</title>
        </Head>
        <Navbar>
          <Navbar.Brand className="mr-auto" href="#home">
            Water Quality Forecasting
          </Navbar.Brand>
          <Button variant="success" onClick={() => this.handleShow()}>Add Excel Data</Button>
        </Navbar>
        <Card style={{ padding: '2rem' }}>
          {dataLoaded ?
            <React.Fragment>
              {/* <Table striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Value</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    dates && targets && Object.keys(dates).map(i => (
                      <tr key={i}>
                        <td>{dates[i]}</td>
                        <td>{targets[i]}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </Table> */}
              <h2>Overall Data</h2>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  {activeFilter ? activeFilter : 'Filter Results'}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  {datasetsRaw.map(i => (
                    <Dropdown.Item key={uuidv4()} onClick={() => this.setFilter(i.label)}>{i.label}</Dropdown.Item>
                  ))}
                  <Dropdown.Item key="all" onClick={() => this.resetFilter()}>Select All</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>

              <div style={{ height: 600 }}>
                <Line style={{ height: 300 }} data={chartDataOverAll} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <hr />
              <h3>Simple Moving Average Data</h3>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataDOSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataBODSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataTSSSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataTempSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataTDSSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 600 }}>
                <Line style={{ height: 600 }} data={chartDatapHMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataColorSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataFecalColiformSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataTotalColiformSMA} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
              <hr />
              <h3>Forecasting</h3>
              <div style={{ height: 300 }}>
                <Line style={{ height: 300 }} data={chartDataBlue} options={{
                  responsive: true,
                  maintainAspectRatio: false,
                }} />
              </div>
            </React.Fragment> :
            <React.Fragment>
              <p>Please upload an excel file fo processing.</p>
            </React.Fragment>
          }
        </Card>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add Excel Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>File</p>
            <input type="file" onChange={this.fileSelect} />

            <p style={{ marginTop: '2em' }}>Details</p>
            <InputGroup className="my-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Parameters</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Extra Arguments"
                aria-label="Extra Arguments"
                aria-describedby="basic-addon1"
              />
            </InputGroup>
            {selectedFile && uploading && <React.Fragment>
              <ProgressBar now={progress} />
            </React.Fragment>}

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.handleUpload}>
              Upload
            </Button>
          </Modal.Footer>
        </Modal>

      </div >
    )
  }
}