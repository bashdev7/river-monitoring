<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
require './vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;


$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["file"]["name"]);

function valtoArray($cell) {
    foreach ($cell as $key => $value) {
        return array($value);
    }
}

function floatValtoArray($cell) {
    foreach ($cell as $key => $value) {
        return floatval($value);
    }
}

// Working example
function read_excel_data($file) {

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
    // Reader
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
    $reader->setReadDataOnly(TRUE);
    $spreadsheet = $reader->load($file);

    $datesArray = $spreadsheet->getActiveSheet()
            ->rangeToArray(
            'A2:A432', // The worksheet range that we want to retrieve
            NULL, // Value that should be returned for empty cells
            FALSE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
            FALSE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
            TRUE         // Should the array be indexed by cell row and cell column
    );

    $DOTargetsArray = $spreadsheet->getActiveSheet()
            ->rangeToArray(
            'B2:B432', // The worksheet range that we want to retrieve
            NULL, // Value that should be returned for empty cells
            TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
            TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
            TRUE         // Should the array be indexed by cell row and cell column
    );

    $BODTargetsArray = $spreadsheet->getActiveSheet()
    ->rangeToArray(
    'C2:C432', // The worksheet range that we want to retrieve
    NULL, // Value that should be returned for empty cells
    TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
    TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
    TRUE         // Should the array be indexed by cell row and cell column
);

$TSSTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'D2:D432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);

$TempTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'E2:E432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);

$TDSTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'F2:F432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);

$pHTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'G2:G432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);


$ColorTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'H2:H432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);

$FecalColiformTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'I2:I432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);

$TotalColiformTargetsArray = $spreadsheet->getActiveSheet()
->rangeToArray(
'J2:J432', // The worksheet range that we want to retrieve
NULL, // Value that should be returned for empty cells
TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
TRUE         // Should the array be indexed by cell row and cell column
);


    return array(
        json_encode(array_map('valtoArray', $datesArray)),
        json_encode(array_map('floatValtoArray', $DOTargetsArray)),
        json_encode(array_map('floatValtoArray', $BODTargetsArray)),
        json_encode(array_map('floatValtoArray', $TSSTargetsArray)),
        json_encode(array_map('floatValtoArray', $TempTargetsArray)),
        json_encode(array_map('floatValtoArray', $TDSTargetsArray)),
        json_encode(array_map('floatValtoArray', $pHTargetsArray)),
        json_encode(array_map('floatValtoArray', $ColorTargetsArray)),
        json_encode(array_map('floatValtoArray', $FecalColiformTargetsArray)),
        json_encode(array_map('floatValtoArray', $TotalColiformTargetsArray)),        
    );
}

$uploadOk = 1;

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        $uploadedFile = basename( $_FILES["file"]["name"]);
        $result = read_excel_data('uploads/'.$uploadedFile);
        $jsonString ='';
        $jsonString.='{"values":{';
        $jsonString.='"dates":[' . $result[0] . '],';
        $jsonString.='"DOTargets":[' . $result[1] . '],';
        $jsonString.='"BODTargets":[' . $result[2] . '],';
        $jsonString.='"TSSTargets":[' . $result[3] . '],';
        $jsonString.='"TempTargets":[' . $result[4] . '],';
        $jsonString.='"TDSTargets":[' . $result[5] . '],';
        $jsonString.='"pHTargets":[' . $result[6] . '],';
        $jsonString.='"ColorTargets":[' . $result[7] . '],';
        $jsonString.='"FecalColiformTargets":[' . $result[8] . '],';
        $jsonString.='"TotalColiformTargets":[' . $result[9] . ']';
        $jsonString.='}}';
        echo $jsonString;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>